<?php
/*
Plugin Name: Tutor LMS Tutorlist
Plugin URI: https://bitbucket.org/circlebd/tutor-lms-tutorlist/
Description: A plugin to get list of Tutors
Version: 1.0.0
Author: Mahfuzul Alam
Author URI: https://www.linkedin.com/in/mahfuzul-alam/
License: GPL2
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (in_array('tutor/tutor.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    add_action('init', 'tutorlist_add_shortcodes');
}

function tutorlist_add_shortcodes()
{
    add_shortcode('tutorlms-tutorlist', 'get_tutorlms_tutorlist');
}

function get_tutorlms_tutorlist($atts)
{
    $args = array(
        'role'    => 'tutor_instructor',
        'meta_key' => '_tutor_instructor_status',
        'meta_value' => 'approved',
    );

    $instructors = get_users($args);

    ob_start();
?>
    <h2>Instructor List</h2>
    <?php
    foreach ($instructors as $instructor) {
        $course_count = tutor_utils()->get_course_count_by_instructor($instructor->ID);
        $instructor_rating = tutor_utils()->get_instructor_ratings($instructor->ID);
    ?>
        <div style="margin-bottom: 20px">
            <h4><a href="<?php echo tutor_utils()->profile_url($instructor->ID); ?>" target="_blank"><?php echo $instructor->display_name; ?></a></h4>
            <p><?php echo $course_count . ' Courses '; ?></p>
            <p><?php echo $instructor_rating->rating_avg . ' (' . $instructor_rating->rating_count . ') Ratings'; ?></p>
        </div>
<?php
    }

    $html = ob_get_contents();
    ob_clean();
    echo $html;
}
